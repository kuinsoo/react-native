import React, { Component } from 'react';
import { Alert } from 'react-native';
import * as Location from 'expo-location'
import Axios from 'axios'

import Loading from '../Page/Loading'
import Weather from './Weather';

const API_KEY = '0c17e9dce2c8cab09343b0b66281328f'

export default class MyLocation extends Component {
  state = {
    isLoading: true
  }
  
  getWeather = async (lat, lon) => {
    const { 
      data: {
        main: { temp },
        weather
      } 
    } = await Axios.get(
      `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&cnt=1&APPID=${API_KEY}&units=metric`
    )
    console.log(weather[0].main)
    this.setState({ isLoading:false,
      temp,
      condition: weather[0].main
    })
  }

  getLocation = async () => {
    try {
      // throw Error()
      await Location.requestPermissionsAsync()
      // console.log('response => ', response)
      const {coords: {latitude, longitude}} = await Location.getCurrentPositionAsync()
      this.getWeather(latitude, longitude)
      // console.log('currentPostion => ', coords)
      console.log(latitude, longitude)
      // this.setState({isLoading: false})
    } catch (e) {
      Alert.alert('당신에 위치를 찾을 수 없습니다.', '위치 사용 설정을 허락 해주세요.')
    }
  }
  
  componentDidMount() {
    this.getLocation()
  }
  render() {
    const { isLoading, temp, condition } = this.state
    return (
      isLoading ? <Loading /> : <Weather temp={ temp } condition={ condition } />
      )
    }
}
