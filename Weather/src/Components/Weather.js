import React from 'react'
import PropTypes from 'prop-types'
import { View, Text, StyleSheet } from 'react-native'

export default function Weather ({ temp }) {
     return (
         <View style={ styles.container }>
             <Text>{ temp }º</Text>
         </View>
     )
}

Weather.propTypes = {
    temp: PropTypes.number.isRequired,
    condition: PropTypes.oneOf([
        'Thunderstorm', // 번개
        'Drizzle', // 이슬비
        'Rain', // 비
        'Snow', // 눈
        'Atmosphere', // 기압 high 
        'Clear', // 맑음
        'Clouds', // 구름
        'Haze',
    ]).isRequired 
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})