import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

function Loading() {
    return (
        <View style={styleSheets.container}>
            <Text>KKu Island Wheater</Text>
        </View>
    )
}

const styleSheets = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 30,
        paddingVertical: 100,
        backgroundColor: '#FDF6AA',
    },
    text: {
        fontSize: 30,
        color: '#2c2c2c'
    }
})

export default Loading