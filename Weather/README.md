## 사용된 라이브러리 설치 

##### Install The Command Line Tools
> npm install --global expo-cli

##### Create New Project
> expo init my-project

##### Install a Exop Api : Location
> expo install expo-location

##### Add Axios Module
> yarn add axios

##### Add Prop Types
> yarn add prop-types