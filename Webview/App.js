import React, { Component } from 'react';
import { View } from 'react-native';
import { WebView } from 'react-native-webview';

export default class App extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <WebView
          source={{
            uri:
              'http://nonga.eemo.co.kr/www/login.php',
          }}
        />
      </View>
    );
  }
}