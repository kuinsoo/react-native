import React from 'react'
import { StyleSheet, Text, SafeAreaView } from 'react-native'

const FnSafeAreaView = () => {
    return (
        <SafeAreaView style={styles.container}>
            <Text>Page contant</Text>
        </SafeAreaView>
    )
}

export default FnSafeAreaView

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#333'
    }
})
