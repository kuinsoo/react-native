import React from 'react'
import { 
    ScrollView,
    RefreshControl,
    StyleSheet,
    Text,
    SafeAreaView,
} from 'react-native'
import Constants from 'expo-constants'

function wait(timeout) {
    return new Promise(resolve => {
        setTimeout(resolve, timeout)
    })
}

const FnRefreshControl = () => {
    const [refreshing, setRefreshing] = React.useState(false)

    const onRefresh = React.useCallback(() => {
        setRefreshing(true)

        wait(2000).then(() => {
            setRefreshing(false)
            alert('새로고침중...')
        })
    }, [refreshing])
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView
                contentContainerStyle={styles.scrollView}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }
            >
                <Text>Pull down to RefreshControl indicator</Text>
            </ScrollView>
        </SafeAreaView>
    )
}

export default FnRefreshControl

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: Constants.statusBarHeight,
    },
    scrollView: {
        flex: 1,
        backgroundColor: 'pink',
        alignItems: 'center',
        justifyContent: 'center',
    },
})
