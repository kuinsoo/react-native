import React from 'react'
import { StyleSheet, Button, View, Text, SafeAreaView, Alert } from 'react-native'
import Constants from 'expo-constants'

function Separator() {
    return <View style={ styles.separator } />
}
export default function ExFnButton() {
    console.log(Constants.statusBarHeight)
    return (
        <SafeAreaView style={ styles.container }>
            <View>
                <Text sytle={ styles.title }>
                    The title and onPress handler are required. It is recommended to set
                    accessibilityLabel to help make your app usable by everyone.
                </Text>
            </View>
            <Button 
                title='Press Me' 
                color='#f194ff' 
                onPress={() => Alert.alert('Button with adjusted color pressed')} />
            <Separator />
            <View>
                <Text style={ styles.title }>
                    This layout strategy lets the title define the width of the button.
                </Text>
                <View style={ styles.fixToText }>
                    <Button 
                        title='Left button'
                        onPress={() => Alert.alert('Left button pressed')}
                    />
                    <Button 
                        title='Right Button'
                        onPress={() => Alert.alert('Right button pressed')}
                    />
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: Constants.statusBarHeight,
        marginHorizontal: 16,
    },
    title: {
        textAlign: 'center',
        marginVertical: 8,
    },
    fixToText: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    separator: {
        marginVertical: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
    }
})