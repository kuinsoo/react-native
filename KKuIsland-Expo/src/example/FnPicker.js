import React, { useState } from "react"
import { View, Picker, StyleSheet, Alert } from "react-native"

export default function FnPicker() {
    const [selectedValue, setSelectedValue] = useState('java')
    return (
        <View style={styles.container}>
            <Picker
                selectedValue={selectedValue}
                style={{height:50, width:150}}
                onValueChange={(itemValue, itemIndex) => {
                    setSelectedValue(itemValue)
                    alert(itemIndex)
                }}
            >
                <Picker.Item label='Java' value='java' />
                <Picker.Item label='JavaScript' value='js' />
            </Picker>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 40,
        alignItems: 'center',
    }
})