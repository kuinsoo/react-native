import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import ExFnActivityIndicator from './src/example/FnActivityIndicator'
import ExCaActivityIndicator from './src/example/CaActivityIndicator'
import ExFnButton from './src/example/FnButton'
import FnFlatList from './src/example/FnFlatList'
import FnFlatListSelectable from './src/example/FnFlatListSelectable'
import FnImage from './src/example/FnImage'
import FnImageBackground from './src/example/FnImageBackground'
import FnKeyboardAvoidingView from './src/example/FnKeyboardAvoidingView' // 가상키보드
import FnModal from './src/example/FnModal' // 함수형 모달
import CaModal from './src/example/CaModal' // 클래스형 모달
import FnPicker from './src/example/FnPicker' // Picker 
import FnRefreshControl from './src/example/FnRefreshControl' // 새로고침 컨트롤러
import FnSafeAreaView from './src/example/FnSafeAreaView' // SafeAreaView
import FnScrollView from './src/example/FnScrollView' // FnScrollView
import FnSectionList from './src/example/FnSectionList' // 섹션 목록 (함수)
import CaSectionList from './src/example/CaSectionList' // 섹션 목록 (클래스)
import FnStatusBar from './src/example/FnStatusBar' // 섹션 목록 (클래스)


export default function App() {
  return (
    <View style={styles.container}>
      {/* <ExFnActivityIndicator /> */}
      {/* <ExCaActivityIndicator /> */}
      {/* <ExFnButton /> */}
      {/* <FnFlatList /> */}
      {/* <FnFlatListSelectable /> */}
      {/* <FnImage /> */}
      {/* <FnImageBackground /> */}
      {/* <FnKeyboardAvoidingView /> */}
      {/* <FnModal /> */}
      {/* <CaModal /> */}
      {/* <FnPicker /> */}
      {/* <FnRefreshControl /> */}
      {/* <FnSafeAreaView /> */}
      {/* <FnScrollView /> */}
      {/* <FnSectionList /> */}
      {/* <CaSectionList /> */}
      <FnStatusBar />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
